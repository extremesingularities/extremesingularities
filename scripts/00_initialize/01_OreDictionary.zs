// --- Import
import crafttweaker.item.IItemStack;



// --- Remove
## Wood Gear
val woodGear = <ore:gearWood>;
woodGear.remove(<enderio:item_material:9>);
woodGear.remove(<appliedenergistics2:material:40>);

## Stone Gear
val gearStone = <ore:gearStone>;
gearStone.remove(<exnihilocreatio:item_material:7>);
gearStone.remove(<enderio:item_material:10>);



// --- Add
## Enderium Nugget
val nuggetEnderium = <ore:nuggetEnderium>;
nuggetEnderium.add(<extendedcrafting:material:37>);

## Enderium Ingot
val ingotEnderium = <ore:ingotEnderium>;
ingotEnderium.add(<extendedcrafting:material:36>);

## Enderium Block
val blockEnderium = <ore:blockEnderium>;
blockEnderium.add(<extendedcrafting:storage:5>);
