// --- Hide
## Singularity
mods.jei.JEI.hide(<avaritia:singularity:*>);

## Neutronium Compressor
mods.jei.JEI.hide(<avaritia:neutronium_compressor>);