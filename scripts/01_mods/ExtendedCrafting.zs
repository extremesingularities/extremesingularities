// --- Remove
## Nugget
recipes.remove(<extendedcrafting:material:37>);

## Ingot
recipes.remove(<extendedcrafting:material:36>);

## Block
recipes.remove(<extendedcrafting:storage:5>);



// --- Add
## Ender Star
recipes.addShaped(<extendedcrafting:material:40>, [
    [null, <ore:ingotEnderium>, null],
    [<ore:ingotEnderium>, <minecraft:nether_star>, <ore:ingotEnderium>],
    [null, <ore:ingotEnderium>, null]
]);
